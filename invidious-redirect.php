<?php

/**
 * Semplice script PHP che reindirizza a una istanza Invidious casuale
 */


// Recupero JSON
$lista = file_get_contents('https://api.invidious.io/instances.json?pretty=1&sort_by=type,users');

// Verifica se il contenuto è in formato JSON
if (isJson($lista)) {
    $istanze = json_decode($lista, true);

    // Recupero lista per tipologia, per utilizzare solo quelle in https (non onion)
    $istanze_per_tipo = [];

    foreach ($istanze as $indice => $istanza) {
        if ($istanza[1]['type'] == 'https') {
            $istanze_per_tipo[] = $istanza[0];
        }
    }

    // selezione lista casuale
    $indirizzo_casuale = $istanze_per_tipo[ rand(0, count($istanze_per_tipo)-1) ];

    header('Location: https://'.$indirizzo_casuale);
    exit();
}

// Se il JSON non è valido o il server non risponde...??
else {
    exit();
}


// Funzione per la verifica del formato JSON
function isJson($string) {
    json_decode($string);
    return json_last_error() === JSON_ERROR_NONE;
}